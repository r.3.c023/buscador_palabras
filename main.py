from fastapi import FastAPI
import uvicorn
import mock

app = FastAPI()

def buscador(): 
    """
    Esta función realiza una búsqueda en los datos simulados de 'mock.tutela' 
    y crea un índice de palabras clave relacionadas con ciertos temas.

    Returns:
        dict: Un diccionario de índices donde las claves son palabras clave y los valores son listas de documentos relacionados.
    """  
    indices = {}
    filter = ["violencia", "estafa", "alimentos", "salud", "pensión", "medicinas"]
    
    for tutela in mock.tutela:
        palabras = tutela["resumen"].split()        
     
        for palabra in palabras:
            if palabra in filter:
                if palabra in indices:
                    indices[palabra].append(tutela)
                else:
                    indices[palabra] = [tutela]
                    
    return indices

index = buscador()

@app.get("/")
def read_root(): 
    """
    Endpoint de bienvenida que devuelve un mensaje simple.

    Returns:
        dict: Un mensaje de bienvenida.
    """
    
    return { "message": "Bienvenido al buscador"}

@app.get("/buscar/{palabra}")

def buscar_palabra(palabra: str):
    """
    Busca una palabra clave en los índices creados por la función 'buscador()'.

    Args:
        palabra (str): La palabra clave a buscar.

    Returns:
        dict: Los documentos relacionados con la palabra clave o un mensaje de error si la palabra no se encuentra.
    """
    resultado = index.get(palabra, "No se encontró la palabra en los índices")
    return {"resultados": resultado}
